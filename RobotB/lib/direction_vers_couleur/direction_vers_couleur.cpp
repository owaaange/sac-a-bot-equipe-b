/**
* \file   direction_vers_couleur.cpp
* \author Team Sac-A-Bot
* \date   04 novembre 2019
* \brief  Librairie des fonctions permettant de se déplacer vers les couleurs
* \version 1.0
*/

#include "direction_vers_couleur.h"
#include <fonctions_moteurs.h>
#include <suiveur_ligne.h>

void bleu(){
tourner_droite_luimeme(45);
delay(100);
     while(est_perpendiculaire() == 0)
     {
         MOTOR_SetSpeed(0,0.25);
         MOTOR_SetSpeed(1,0.25);
     }
ligne_droite(1);
tourner_droite_luimeme(75);
ligne_droite(40);
}

void jaune(){

    tourner_gauche_luimeme(45);
    delay(100);
    while(est_perpendiculaire() == 0)
    {
        MOTOR_SetSpeed(0,0.25);
        MOTOR_SetSpeed(1,0.25);
    }
ligne_droite(1);
tourner_gauche_luimeme(90);

ligne_droite(40);

}

void vert(){

tourner_droite_luimeme(90);
ligne_droite(55);
tourner_gauche_luimeme(90);
ligne_droite(68);
tourner_gauche_luimeme(45);

while(!est_perpendiculaire())
{
MOTOR_SetSpeed(0,0.25);
MOTOR_SetSpeed(1,0.25);
}

ligne_droite(1);
tourner_droite_luimeme(90);

ligne_droite(15);
}

void rouge(){

tourner_gauche_luimeme(90);
ligne_droite(55);
tourner_droite_luimeme(90);
ligne_droite(68);
tourner_droite_luimeme(45);

while(!est_perpendiculaire())
{
MOTOR_SetSpeed(0,0.25);
MOTOR_SetSpeed(1,0.25);
}

ligne_droite(1);
tourner_gauche_luimeme(90);

ligne_droite(15);
}

void retour_rouge(){

        ligne_droite(30);
        tourner_droite_luimeme(45);
        ligne_droite(60);
        tourner_gauche_luimeme(45);

}