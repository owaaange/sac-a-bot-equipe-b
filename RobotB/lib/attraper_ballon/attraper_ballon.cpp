#include <Arduino.h>
#include <math.h>
#include <LibRobus.h>
#include "attraper_ballon.h"

/**
*  \brief Attraper le ballon et vérifier si il est toujours dans la pelle
* 
*  Attraper le ballon tout en reculant tranquilement 
*  Si les deux capteurs captent le ballon dans la pelle, continuer les procédure pour amener le ballon au centre
*  Si un des deux capteurs ne captent plus le ballon dans la pelle, recommencer le processus d'attraper le ballon
* 
*  SERVO_Enable(uint8_t id) / SERVO_Disable(uint8_t id)
*  SERVO_SetAngle(uint8_t id, uint8_t angle)
*
*/

void attrape_ballon()
{

  SERVO_Enable(0);
  SERVO_SetAngle(0, 0);
  delay(1000);
  //SERVO_Disable(0);
}

void relacher_ballon()
{

  SERVO_Enable(0);
  SERVO_SetAngle(0, 50);
  delay(1000);
  SERVO_Disable(0);
}

/**
 *  \brief Attraper le ballon et vérifier si il est toujours dans la pelle
 * 
 *  Attraper le ballon tout en reculant tranquilement 
 *  Si les deux capteurs captent le ballon dans la pelle, continuer les procédure pour amener le ballon au centre
 *  Si un des deux capteurs ne captent plus le ballon dans la pelle, recommencer le processus d'attraper le ballon
 * 
 */

// void attrape_ballon()
// {
//   Servo1.write(35); //angle fait par le servo
//   delay(2000);  //delay est en ms pour le temps que prend le servo à faire l'angle
//   while (delay!=2000) {
//     MOTOR_SetSpeed(0,0.20); //recule lentement durant la descente de la pelle
//     MOTOR_SetSpeed(1,0.20);
//   }
//   if () { //switch pelle est activée! Nous avons le ballon, va falloir que je comprenne comment la prog du capteur sonar marche... if (sonar+switch sont à ON)
//     //amener au centre
//   } else { //switch pelle n'est pas activée! Perte du ballon. if (sonar OU switch est à OFF)
//     //recommencer recherche ballon
//   }  
// }
