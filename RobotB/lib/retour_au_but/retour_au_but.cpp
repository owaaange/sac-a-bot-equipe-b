/**
* \file   retour_au_but.cpp
* \author Team Sac-A-Bot
* \date   06 novembre 2019
* \brief  Librairie des fonctions permettant de se retourner vers les buts de couleurs différentes
* \version 1.0
*/

#include "retour_au_but.h"

void retour_au_but(Couleur c)
{
    switch(c)
    {
        case Jaune:
            ligne_droite(40);
            tourner_droite_luimeme(90);
            ligne_droite(15);
            tourner_droite_luimeme(45);
            
            while(est_perpendiculaire() == 0)
            {
                MOTOR_SetSpeed(0, 0.25);
                MOTOR_SetSpeed(1, 0.25);
            }

            ligne_droite(1);
            tourner_gauche_luimeme(90);
            ligne_droite(80);
            break;

        case Vert:
            reculer_ligne_droite(40);
            tourner_gauche_luimeme(90);
            ligne_droite(15);
            tourner_droite_luimeme(45);
            
            while(est_perpendiculaire() == 0)
            {
                MOTOR_SetSpeed(0, 0.25);
                MOTOR_SetSpeed(1, 0.25);
            }

            ligne_droite(1);
            tourner_gauche_luimeme(90);
            ligne_droite(80);
            break;

        case Bleu:
            ligne_droite(40);
            tourner_gauche_luimeme(90);
            ligne_droite(15);
            tourner_gauche_luimeme(45);
            
            while(est_perpendiculaire() == 0)
            {
                MOTOR_SetSpeed(0, 0.25);
                MOTOR_SetSpeed(1, 0.25);
            }

            ligne_droite(1);
            tourner_droite_luimeme(90);
            ligne_droite(80);
            break;

        case Rouge:
            reculer_ligne_droite(40);
            tourner_droite_luimeme(90);
            ligne_droite(15);
            tourner_gauche_luimeme(45);
            
            while(est_perpendiculaire() == 0)
            {
                MOTOR_SetSpeed(0, 0.25);
                MOTOR_SetSpeed(1, 0.25);
            }

            ligne_droite(1);
            tourner_droite_luimeme(90);
            ligne_droite(80);
            break;
    }

    relacher_ballon();
}