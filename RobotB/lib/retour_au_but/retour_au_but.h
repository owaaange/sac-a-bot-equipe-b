#ifndef RETOURAUBUT_H
#define RETOURAUBUT_H

#include <LibRobus.h>
#include "fonctions_moteurs.h"
#include "suiveur_ligne.h"
#include "attraper_ballon.h"
#include <math.h>

enum Couleur
{
    Jaune,
    Vert,
    Bleu,
    Rouge
};

void retour_au_but(Couleur c);

#endif