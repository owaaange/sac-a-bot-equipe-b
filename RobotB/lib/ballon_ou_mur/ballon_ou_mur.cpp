/*
Projet: Le nom du script
Equipe: Votre numero d'equipe
Auteurs: Les membres auteurs du script
Description: Breve description du script
Date: Derniere date de modification
*/

/* ****************************************************************************
Inclure les librairies de functions que vous voulez utiliser
**************************************************************************** */

#include <LibRobus.h> // Essentielle pour utiliser RobUS
#include <math.h>
#include "ballon_ou_mur.h"
#include "fonctions_moteurs.h"
double valvoltbas;
double distancebas;
double valvolthaut;
double distancehaut;
int trouverballon();
double distancecapteurhaut();
double distancecapteurbas();
int ballonoumur();

/* ****************************************************************************
Variables globales et defines
**************************************************************************** */
// -> defines...
// L'ensemble des fonctions y ont acces

/* ****************************************************************************
Vos propres fonctions sont creees ici
**************************************************************************** */

/* cette fonction returne soit 1, soit 0
*si la distance est égale = cest un mur
*si elle n'est pas égale alors = c'est un ballon
*/

double distancecapteurbas()
{
  valvoltbas = ((((double)ROBUS_ReadIR(1)) * 5) / 1023);
  //Serial.println("valvoltbas");
  //Serial.println(ROBUS_ReadIR(0));
  distancebas = 27.762 * pow(valvoltbas, -1.2045);
  return distancebas;
}

double distancecapteurhaut()
{
  valvolthaut = ((((double)ROBUS_ReadIR(2)) * 5) / 1023);
  //Serial.println("valvolthaut");
  //Serial.println(ROBUS_ReadIR(2));
  distancehaut = 27.762 * pow(valvolthaut, -1.2045);
  return distancehaut;
}

double moyenne_distancebas_chop16()
{
  double total = 0;
  double moy = 0;
  double x;
  double result = 14;

  for (int i = 0; i < 25; i++)
  {

    x = chop16();
    //  Serial.print("chop x ");
    //   Serial.println(x);
    total += x;
  }
  result = total / 25;
  Serial.print("result");
  Serial.println(result);
  return result;
}

double chop16()
{
  double x = 0;
  if ((x = distancecapteurbas()) > 16)
    chop16();
  else
  {

    return x;
  }
}

double moyenne_distancebas()
{
  double total = 0;
  double moy = 0;

  for (int i = 0; i < 25; i++)
  {

    total += distancecapteurbas();
  }

  moy = total / 25;

  return moy;
}

int ballonoumur()
//distance 1 et 2 = distances capteurs
{
  delay(5);
  distancebas = distancecapteurbas();
  distancehaut = distancecapteurhaut();
  if (distancebas < (distancehaut - 12))
  { // ici - 12 est pour donnée la marge d'erreur potentiel des capteurs
    return 1;
  }

  else
  {
    return 0;
  }
}

int trouver_ballon_ligne()
{

  tourner_droite_luimeme(3);
  if (un_ou_zero())
  {
    return 0;
  }
  tourner_gauche_luimeme(6);
  if (un_ou_zero())
  {
    return 0;
  }
  tourner_droite_luimeme(9);
  if (un_ou_zero())
    return 0;
  tourner_gauche_luimeme(12);
  if (un_ou_zero())
    return 0;
  tourner_droite_luimeme(15);
  if (un_ou_zero())
    return 0;
  tourner_gauche_luimeme(18);
  if (un_ou_zero())
    return 0;
  tourner_droite_luimeme(21);
  if (un_ou_zero())
    return 0;
  tourner_gauche_luimeme(24);
  if (un_ou_zero())
    return 0;
  tourner_droite_luimeme(27);
  if (un_ou_zero())
    return 0;
  tourner_gauche_luimeme(30);
  if (un_ou_zero())
    return 0;
  tourner_droite_luimeme(33);
  if (un_ou_zero())
    return 0;
  tourner_gauche_luimeme(36);
  if (un_ou_zero())
    return 0;
  tourner_droite_luimeme(39);
  if (un_ou_zero())
    return 0;
  tourner_gauche_luimeme(42);
  if (un_ou_zero())
    return 0;
  tourner_droite_luimeme(45);
  if (un_ou_zero())
    return 0;
  tourner_gauche_luimeme(48);
  if (un_ou_zero())
    return 0;
  tourner_droite_luimeme(39);
  if (un_ou_zero())
    return 0;
  tourner_gauche_luimeme(51);
  if (un_ou_zero())
    return 0;
  return 0;
}
int trouverballon()
{
  int count = 0;
  while (!un_ou_zero())
  {
    tourner_droite_luimeme(2);
    count = +1;
    if (count == 15)
      break;
    delay(5);
  }

  delay(2000);
  tourner_droite_luimeme(2);

  if (!un_ou_zero())
  {
    tourner_gauche_luimeme(30);

    while (!un_ou_zero())
    {
      tourner_gauche_luimeme(2);
      count = +1;
      delay(5);
    }
  }

  return 0;
}

int un_ou_zero()
{
  int stock[51];
  int i;
  int compteur_ballon = 0;
  int compteur_mur = 1;

  for (i = 0; i < 51; i++)
  {
    stock[i] = ballonoumur();
  }

  for (i = 0; i < 51; i++)
  {
    if (stock[i] == 0)
    {
      compteur_mur += 1;
    }
    if (stock[i] == 1)
    {
      compteur_ballon += 1;
    }
  }
  if (compteur_ballon > compteur_mur)
  {
    return 1;
  }
  else if (compteur_ballon < compteur_mur)
  {
    return 0;
  }
  else
  {
    un_ou_zero();
  }
  return 0;
}

int reach_ballon()
{

  while (moyenne_distancebas() > 16)
  {
    ligne_droite(5);
    //  Serial.println("distancecapteurbas");
    //  Serial.println(distancecapteurbas());
    if (!un_ou_zero())
      trouver_ballon_ligne();
    delay(20);
  }
  plus_pres();
  ligne_droite(moyenne_distancebas_chop16() - 3);

  return 1;
}

int plus_pres()
{
  double x = moyenne_distancebas();

  tourner_droite_luimeme(2);
  delay(500);
  if (moyenne_distancebas() < x)
  {
    Serial.print("x");
    Serial.println(x);
    Serial.print("moyenne_distance");
    Serial.println(moyenne_distancebas());
    return 1;
  }
  else
    tourner_gauche_luimeme(4);
  delay(500);
  if (distancecapteurbas() < x)
  {
    return 1;
  }
  else
  {
    tourner_droite_luimeme(2);
  }
  return 0;
}
