#include "parcours_A.h"

void parcours_rouge(){

relacher_ballon();

   //appeler la couleur ou aller chercher le ballon (vert, bleu, orange ou jaune) puis suivre la ligne
  rouge();

    //s'orienter vers le ballon 
 trouver_ballon_ligne();
    
    //avancer vers le ballon
  reach_ballon();

    //attraper le ballon 
  attrape_ballon();
  
  tourner_gauche_luimeme(180);

    ligne_droite(40);
    
    tourner_droite(45);
    
    ligne_droite(30);
    
    tourner_gauche(90);
    
    ligne_droite(17);
    
    delay(800);
    
    relacher_ballon();
    
    delay(500);
    
    reculer_ligne_droite(75);
    
    tourner_droite_luimeme(90);


}

void parcours_vert(){

relacher_ballon();

   //appeler la couleur ou aller chercher le ballon (vert, bleu, orange ou jaune) puis suivre la ligne
  vert();

    //s'orienter vers le ballon 
 trouver_ballon_ligne();
    
    //avancer vers le ballon
  reach_ballon();

    //attraper le ballon 
  attrape_ballon();
  
  tourner_gauche_luimeme(180);

    ligne_droite(44);
    
    tourner_gauche(45);
    
    ligne_droite(28);
    
    tourner_droite(90);
    
    ligne_droite(15);
    
    delay(500);
    
    relacher_ballon();
    
    delay(500);
    
    reculer_ligne_droite(75);
    
    tourner_droite_luimeme(90);



}

void parcours_jaune(){

relacher_ballon();

jaune();

trouver_ballon_ligne();

reach_ballon();

attrape_ballon();

tourner_gauche_luimeme(180);

ligne_droite(84);

delay(500);

relacher_ballon();

delay(500);

reculer_ligne_droite(40);

tourner_gauche_luimeme(45);

ligne_droite(30); 

tourner_droite_luimeme(90);

reculer_ligne_droite(60);

tourner_droite_luimeme(90);

}

void parcours_bleu(){

relacher_ballon();

bleu();

trouver_ballon_ligne();

reach_ballon();

attrape_ballon();

tourner_gauche_luimeme(180);

ligne_droite(84);

delay(500);

relacher_ballon();

delay(500);

reculer_ligne_droite(40);

tourner_gauche_luimeme(45);

ligne_droite(30); 

tourner_droite_luimeme(90);

reculer_ligne_droite(50);

tourner_droite_luimeme(90);

}