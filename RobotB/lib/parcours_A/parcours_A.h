#ifndef PARCOURS_A_H
#define PARCOURS_A_H

#include <LibRobus.h> // Essentielle pour utiliser RobUS
#include <math.h>     //Importe les fonctions de mathématique

#include <PID.h>   //Importe les fonctions du programme PID.c

#include <fonctions_moteurs.h> //Importe les fonctions des moteurs 
    //(tourner, avancer ligne droite, acceleration...)
#include <fonctions_capteurs.h> //Importe les fonctions qui gérent les 
    //différents capteurs
#include <suiveur_ligne.h> //Importe les fonctions qui permettent de suivre des
    //lignes
#include <attraper_ballon.h> //Importe les fonctions qui permettent d'attraper
    //le ballon
#include <ballon_ou_mur.h>

#include <direction_vers_couleur.h>


void parcours_rouge();
void parcours_vert();
void parcours_jaune();
void parcours_bleu();

#endif