/**
  *Projet: Sac-à-Bot
  *Equipe: P-01
  *Auteurs: Les membres auteurs du script
  *Description: Breve description du script
  *Date: Derniere date de modification
*/

/* ****************************************************************************
Inclure les librairies de functions que vous voulez utiliser
**************************************************************************** */

#include <LibRobus.h> // Essentielle pour utiliser RobUS
#include <math.h>     //Importe les fonctions de mathématique
#include <PID.h>   //Importe les fonctions du programme PID.c
#include <fonctions_moteurs.h> //Importe les fonctions des moteurs 
    //(tourner, avancer ligne droite, acceleration...)
#include <fonctions_capteurs.h> //Importe les fonctions qui gérent les 
    //différents capteurs
#include <suiveur_ligne.h> //Importe les fonctions qui permettent de suivre des
    //lignes
#include <attraper_ballon.h> //Importe les fonctions qui permettent d'attraper
    //le ballon
#include <ballon_ou_mur.h>
#include <direction_vers_couleur.h>

#include "parcours_A.h"

#include"retour_au_but.h"
/* ****************************************************************************
Variables globales et defines
**************************************************************************** */
// -> defines...
// L'ensemble des fonctions y ont acces

/* ****************************************************************************
Vos propres fonctions sont creees ici
**************************************************************************** */

int compteurTimer = 0;	//Offset pour compenser

void change_LED_state()
{
	char val = digitalRead(LED_BUILTIN);
	digitalWrite(LED_BUILTIN, !val);
	compteurTimer++;
}


/**
*   /breif : trouve le chemin le plus rapide pour se rendre à la couleur désirée
*   /param :'couleur_finale' est sa destination
*        
*/
void trouver_couleur( int couleur_finale)
{
    //jaune : 1
    //rouge : 2
    //vert :  3
    //bleu :  4

    switch(couleur_finale)
    {
        case 1:
        {
            //derriere gauche

            demi_tour();  
            avancer();  
            while(analogRead(A0)!=0)     //trouver la ligne
            {}
            MOTOR_SetSpeed(0,0);
            MOTOR_SetSpeed(1,0);
            tourner_droite(90);
            //suivre la ligne jusqu'à la couleur
            break; 
        }

        case 2:
        {
            //avant gauche
             
            avancer();  
            while(analogRead(A0)!=0)     //trouver le cercle
            {}
            MOTOR_SetSpeed(0,0);
            MOTOR_SetSpeed(1,0);
            tourner_gauche(45);
            //suivre la ligne jusqu'à la couleur
            
            break;
        }

        case 3:
        {
            //avant droite

             avancer();  
            while(analogRead(A0)!=0)     //trouver le cercle
            {}
            MOTOR_SetSpeed(0,0);
            MOTOR_SetSpeed(1,0);
            tourner_droite(45);
            //suivre la ligne jusqu'à la couleur
            break;
        }

        case 4:
        {
            //derriere droite
             
            demi_tour();  
            avancer();  
            while(analogRead(A0)!=0)     //trouver la ligne
            {}
            MOTOR_SetSpeed(0,0);
            MOTOR_SetSpeed(1,0);
            tourner_gauche(90);
            //suivre la ligne jusqu'à la couleur
            break;
        }
    }



}


/**
*   /breif : stop les moteurs et met le robot dans une boucle infini
*   /param 
*   /return void
*/

void temps_ecoule()
{
  MOTOR_SetSpeed(1,0);
  MOTOR_SetSpeed(1,0);

  while (1)
  { }
}



/* ****************************************************************************
Fonctions d'initialisation (setup)
**************************************************************************** */
// -> Se fait appeler au debut du programme
// -> Se fait appeler seulement une fois
// -> Generalement on y initialise les variables globales

void setup()
{
    BoardInit();
	SOFT_TIMER_SetCallback(0, change_LED_state);
	SOFT_TIMER_SetDelay(0, 500);
	SOFT_TIMER_SetRepetition(0, 120);

	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, 0);

	pinMode(A15, INPUT);

	//Set la pince
	attrape_ballon();
}

/* ****************************************************************************
Fonctions de boucle infini (loop())
**************************************************************************** */
// -> Se fait appeler perpetuellement suite au "setup"

void loop()
{
    double CIRCONFERENCE_ROUE_28 = M_PI * 7.55;
    int longueur = 28;
    float nbr_tour_roue = longueur / CIRCONFERENCE_ROUE_28;
    int pulse_cible = round(nbr_tour_roue * 3200);
    // attrape_ballon();
    while(!ROBUS_IsBumper(3)){
         // Do nothing
     }

    //Decommenter pour timer 60 sec.
	// SOFT_TIMER_Enable(0);
	// //Attend une minute
	// while(compteurTimer < 120)
	// {
	// 	SOFT_TIMER_Update();
	// 	delay(1);
	// }
    // SOFT_TIMER_Disable(0);
    digitalWrite(LED_BUILTIN, 1);	//Allume la DEL

    relacher_ballon();				//Ouvre la pince
    
    trouver_ballon_ligne();
    reach_ballon();
    attrape_ballon();
    compenser_angle();
    retour_au_but(Vert);

  delay(1000); // Delais pour décharger le CPU
}
